import requests
import os.path
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.firefox import GeckoDriverManager
from time import sleep
import pathlib


"""Ripper class to download a pdf from an overleaf instance"""
class Ripper:
    url = ""
    project = ""
    cookies = None
    downloadpath = ""

    def __init__(self, baseurl, projectId) -> None:
        self.url = baseurl
        self.project = projectId

        """Logs in to overleaf and prepares download

        :param email: the email address of the user
        :param password: corresponding password
        :param downloadPath: path for the pdf file
        """
    def ripSel(self, email, password, downloadPath) -> None:
        self.downloadpath = downloadPath
        service = FirefoxService()
        profile = webdriver.FirefoxProfile()
        options = webdriver.FirefoxOptions()
        options.add_argument("--headless")
        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.download.dir", downloadPath)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
        profile.set_preference("pdfjs.disabled", True)
        driver = webdriver.Firefox(service=service, firefox_profile=profile, options=options)
        print("======> {}".format(self.url))
        driver.get("{}".format(self.url))

        nameInput = driver.find_element(By.NAME, "email")
        nameInput.click()
        nameInput.send_keys(email)

        nameInput = driver.find_element(By.NAME, "password")
        nameInput.click()
        nameInput.send_keys(password)

        loginForm = driver.find_element(By.NAME, "loginForm")
        loginForm.submit()

        #TODO: Check if the login was successfull by parsing the seen content
        sleep(5)

        try:
            fail = driver.find_element(By.CSS_SELECTOR, ".alert-danger")
        except:
            fail = None

        if(fail == None):
            driver.get("{}/project/{}".format(self.url, self.project))
            sleep(5)
            compileLabels = driver.find_elements(By.CSS_SELECTOR, ".btn-recompile-label")
            compiling = self.isCompiling(compileLabels)

            while compiling:
                compiling = self.isCompiling(compileLabels)
                print("Compiling {}".format(compiling))
                sleep(3)

            projectnameContainer = driver.find_element(By.CSS_SELECTOR, '.name');
            projectName = projectnameContainer.text.replace(" ", "_")

            projectLink = driver.find_element(By.CSS_SELECTOR, 'a[tooltip="Download PDF"]'.format(self.project))
            print("Looking for the pdf...")
            projectLink.click()
            
            filename = "{}.pdf".format(projectName)
            print("Saving the pdf...")
            self.handleSaving(filename, "output.pdf")

            menubutton = driver.find_element(By.CSS_SELECTOR, '.toolbar-left > a.btn')
            menubutton.click()
            sleep(2)
            zipbutton = driver.find_element(By.CSS_SELECTOR, '.nav-downloads > li:first-of-type > a')
            
            print("Looking for the zip...")
            zipbutton.click()
            filename = "{}.zip".format(projectName)
            print("Saving the zip...")
            self.handleSaving(filename, "output.zip")
        else:
            raise ValueError("Wrong username or password provided. Check your credentials")

        driver.quit()


        """Checks if pdf is still compiling
        """
    def isCompiling(self, elements) -> bool:
        if(elements[0].is_displayed() ):
            return False
        else:
            return True
            

        """Handles download of pdf file

        :param filename: Filename of the pdf
        """
    def handleSaving(self, filename, outputfile) -> None:
        while not os.path.exists(self.downloadpath+"/{}".format(filename)):
            print("Waiting for the file to finish downloading...")
            sleep(3)

        if os.path.isfile(self.downloadpath+"/{}".format(filename)):
            if os.path.isfile(self.downloadpath+"/{}".format(outputfile)):
                os.remove(self.downloadpath+"/{}".format(outputfile))
            
            print("Renaming......")
            os.rename(self.downloadpath+"/{}".format(filename), self.downloadpath+"/{}".format(outputfile))
